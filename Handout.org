#+title: Handout
#+options: toc:nil language:de
#+latex_header: \usepackage{arsclassica}
#+latex_header: \usepackage{amsthm,amssymb,amsmath}
#+latex_header: \usepackage[ngerman]{babel}
#+latex_header: \usepackage[utf8]{inputenc}
#+latex_header: \newcommand\EE{\mathbb{E}}
#+latex_header: \newcommand\PP{\mathbb{P}}
#+latex_header: \newcommand\RR{\mathbb{R}}


* Ungleichungen

 1. \[ \left(\frac{n}{e}\right)^n \leq n! \leq e \sqrt{n} \left(\frac{n}{e}\right)^n \]
 2. \[ \left(\frac{n}{k}\right)^k \leq \binom{n}{k} \leq \left(\frac{en}{k}\right)^k \]
 3. Für kleine \( p \) nützlich (aber für alle \( p \) wahr):
    \[ (1-p)^m \leq e^{-mp} \]
 4. Für \( 0 \leq p \leq \frac{1}{2} \):
    \[ 1 - p \geq e^{-2p} \]

    

* Themen
  1. Geburtstagsparadoxon
  2. Balls and Bins
  3. Binomialverteilung
  4. Poissonverteilung
  5. Poissonapproximation
  6. Couponsammelproblem
  7. Anwendung: Bloom-Filter
  8. Zufallsgraphen
  9. Anwendung: Hamilton-Kreise

